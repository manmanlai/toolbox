package com.example.securityDemo.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: security6
 * @description: 测试-用户接口
 * @author: mml
 * @create: 2024/02/17
 */
@RestController
public class UserController {

    @RequestMapping("/list")
    @PreAuthorize("hasPermission('/list','system:user:list')")
    public String list() {
        return "get list ";
    }

    @RequestMapping("/add")
    @PreAuthorize("hasPermission('/add','system:user:add')")
    public String add() {
        return "post add ";
    }
}
