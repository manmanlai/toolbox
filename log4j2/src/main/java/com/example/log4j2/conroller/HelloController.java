package com.example.log4j2.conroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: logback
 * @description: 测试类
 * @author: mml
 * @create: 2024/02/14
 */
@RestController
public class HelloController {

    public static final Logger logger = LoggerFactory.getLogger(HelloController.class);

    @GetMapping("/hello")
    public String hello() {
        String msg = "hello log4j2";
        logger.info(msg);
        return msg;
    }

}
