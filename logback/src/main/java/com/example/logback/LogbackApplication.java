package com.example.logback;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

@Slf4j
@SpringBootApplication
public class LogbackApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext application = SpringApplication.run(LogbackApplication.class, args);
        ConfigurableEnvironment environment = application.getEnvironment();
        String port = environment.getProperty("server.port");
        String applicationName = environment.getProperty("spring.application.name");
        log.info("\n应用启动：{} \n访问链接为： http://localhost:{}", applicationName, port);
    }

}
